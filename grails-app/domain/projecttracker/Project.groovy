package projecttracker

class Project {
    String name
    String description
    Date dueDate
    String billingType
    String toString () {
        "${name}"
    }
    
    String toStriong () {
        "${billingType}"
    }
    
    static belongsTo = [owner : EndUser]
    static hasMany = [tasks : Task]
    
    static constraints = {
        // blank:false = project name cannot be blank / is required
        name(blank: false, unique: true)
        description()
        dueDate(min: new Date())
        billingType(inList: ["Hourly", "Milestone", "Non-billable"])
    }
}
